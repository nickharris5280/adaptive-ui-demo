//
//  main.m
//  SizeClassLabelDemo
//
//  Created by Nick Harris on 6/19/15.
//  Copyright (c) 2015 Nick Harris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
