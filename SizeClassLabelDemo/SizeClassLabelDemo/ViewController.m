//
//  ViewController.m
//  SizeClassLabelDemo
//
//  Created by Nick Harris on 6/19/15.
//  Copyright (c) 2015 Nick Harris. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, weak) IBOutlet UILabel *traitCollectionDescriptionLabel;

@end

@implementation ViewController


- (void)traitCollectionDidChange:(UITraitCollection *)previousTraitCollection
{
    self.traitCollectionDescriptionLabel.text = [self stringDescriptionForTraitCollection:self.traitCollection];
}

- (NSString *)stringDescriptionForTraitCollection:(UITraitCollection *)traitCollection
{
    NSMutableString *stringDescription = [NSMutableString string];
    
    if(traitCollection.userInterfaceIdiom == UIUserInterfaceIdiomUnspecified)
    {
        [stringDescription appendString:@"UserIterfaceIdiom: Unspecified\n"];
    }
    else if(traitCollection.userInterfaceIdiom == UIUserInterfaceIdiomPad)
    {
        [stringDescription appendString:@"UserIterfaceIdiom: iPad\n"];
    }
    else if(traitCollection.userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        [stringDescription appendString:@"UserIterfaceIdiom: iPhone\n"];
    }
    
    if(traitCollection.horizontalSizeClass == UIUserInterfaceSizeClassUnspecified)
    {
        [stringDescription appendString:@"HorizonalSizeClass: Unspecified\n"];
    }
    else if(traitCollection.horizontalSizeClass == UIUserInterfaceSizeClassRegular)
    {
        [stringDescription appendString:@"HorizonalSizeClass: Regular\n"];
    }
    else if(traitCollection.horizontalSizeClass == UIUserInterfaceSizeClassCompact)
    {
        [stringDescription appendString:@"HorizonalSizeClass: Compact\n"];
    }
    
    if(traitCollection.verticalSizeClass == UIUserInterfaceSizeClassUnspecified)
    {
        [stringDescription appendString:@"VerticalSizeClass: Unspecified"];
    }
    else if(traitCollection.verticalSizeClass == UIUserInterfaceSizeClassRegular)
    {
        [stringDescription appendString:@"VerticalSizeClass: Regular"];
    }
    else if(traitCollection.verticalSizeClass == UIUserInterfaceSizeClassCompact)
    {
        [stringDescription appendString:@"VerticalSizeClass: Compact"];
    }
    
    return stringDescription;
}

@end
