//
//  AppDelegate.h
//  SimpleAutoLayoutDemo
//
//  Created by Nick Harris on 6/19/15.
//  Copyright © 2015 Nick Harris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

